/*
*
* Recursive class
*
* generate random sentences using recursive syntax BNF, Bachus Naur form
*
* ----- example output -----
*
* some big unicorn who believes that a tiny elephant who finds Miss America sees Richard Nixon talks.
*
*
* Miss America sees a tiny fish .
* 
* 
* the woman believes that some bald elephant jumps.
* 
* 
* Fred runs.
* 
* ----- grammar implemented -----
* 
* <sentence> ::= <simple_sentence> [ <conjunction> <sentence> ]
* 
* <simple_sentence> ::= <noun_phrase> <verb_phrase>
* 
* <noun_phrase> ::= <proper_noun> |
* <determiner> [ <adjective> ]. <common_noun> [ who <verb_phrase> ]
* 
* <verb_phrase> ::= <intransitive_verb> |
* <transitive_verb> <noun_phrase> |
* is <adjective> |
* believes that <simple_sentence>
* 
* <conjunction> ::= and | or | but | because
* 
* <proper_noun> ::= Fred | Jane | Richard Nixon | Miss America
* 
* <common_noun> ::= man | woman | fish | elephant | unicorn
* 
* <determiner> ::= a | the | every | some
* 
* <adjective> ::= big | tiny | pretty | bald
* 
* <intransitive_verb> ::= runs | jumps | talks | sleeps
* 
* <transitive_verb> ::= loves | hates | sees | knows | looks for | finds
*
* ----- compile -----
*
* javac Recursive.java
*
* ----- run -----
*
* java Recursive
*/
public class Recursive {

    static final String[] properNouns = { "Fred","Jane","Richard Nixon","Miss America" };
    static final String[] commonNouns = { "man","woman","fish","elephant","unicorn" };
                                   
    static final String[] intransitiveVerbs = {"runs","jumps","talks","sleeps"};
    static final String[] transitiveVerbs = {"loves","hates","sees","knows","looks for","finds"};
    static final String[] determiner = {"a","the","every","some"};
    static final String[] adjective = {"big","tiny","pretty","bald"};
    static final String[] conjunction = {"and","or","but","because"};


    public static void main(String[] args) {
        while (true) {
            sentence();
            System.out.println(".\n\n");
            try {
             Thread.sleep(1000);
            }
            catch (InterruptedException e) {
            }
        }
    }
   
    static void sentence(){ // print a sentence
        double prob;
        int pos;
        pos=(int)(Math.random()*Double.valueOf(conjunction.length));
        prob=Math.random();
        simpleSentence();
        if(prob>0.5){
            System.out.print(conjunction[pos]+" ");
            sentence();
        }
    }
    static void simpleSentence(){ // print a simple sentence, noun phrase verb phrase
        nounPhrase();
        verbPhrase();
    }
    static void nounPhrase(){ // print noun phrase, proper noun
        int pos;
        int posDet;
        int posComm;
        int posAdj;
        double proba;
        double probb;
        double probc;
        pos=(int)(Math.random()*Double.valueOf(properNouns.length));
        posDet=(int)(Math.random()*Double.valueOf(determiner.length));
        posComm=(int)(Math.random()*Double.valueOf(commonNouns.length));
        posAdj=(int)(Math.random()*Double.valueOf(adjective.length));
        proba=Math.random();
        probb=Math.random();
        probc=Math.random();
        if(proba>0.5){
            System.out.print(properNouns[pos]+" ");
        } else {
            System.out.print(determiner[posDet]+" ");
            if(probb>0.5){
                System.out.print(adjective[posAdj]+" ");
            }
            System.out.print(commonNouns[posComm]+" ");
            if(probc>0.5){
                System.out.print("who ");
                verbPhrase();
            }
        }
    }
    static void verbPhrase(){ // print verb phrase, intransitive verb
        int posIntr;
        int posTran;
        int posAdj;
        double prob;
        posIntr=(int)(Math.random()*Double.valueOf(intransitiveVerbs.length));
        posTran=(int)(Math.random()*Double.valueOf(transitiveVerbs.length));
        posAdj=(int)(Math.random()*Double.valueOf(adjective.length));
        prob=Math.random();
        if(prob > 0.75){
            System.out.print(intransitiveVerbs[posIntr]+" ");
        } else if(prob > 0.5) {
            System.out.print("is "+adjective[posAdj]+" ");
        } else if(prob > 0.25) {
            System.out.print("believes that ");
            simpleSentence();
        } else {
            System.out.print(transitiveVerbs[posTran]+" ");
            nounPhrase();
        }
    }
}
